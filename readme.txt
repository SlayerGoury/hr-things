Usage instructions


ex1:

Requirements:
SymPy library (tested with 0.7.6.1)

Usage:
$ python ex1.py "<expression>"

Example:
$ python ex1.py "(x - 5)(2x^3 + x(x^2 - 9))"

Quotation marks are not required but highly recommended


ex2:

Usage:
$ python ex2.py <value> <augend limit>

Example:
$ python ex2.py 150 150


Made for and tested with python 2.7.9
