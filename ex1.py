import sys
import sympy
from sympy.parsing.sympy_parser import parse_expr, standard_transformations, implicit_multiplication_application, convert_xor

hh_type = (standard_transformations + (implicit_multiplication_application, convert_xor,))

expression = parse_expr(sys.argv[1], transformations=hh_type)
print 'Parsed input:'
sympy.pprint(expression)

polynominal = expression.expand()
print '\nPolynominal:'
sympy.pprint(polynominal)
