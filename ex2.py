import sys

n = int(sys.argv[1])
k = int(sys.argv[2])

# force terms of hh ex
if n>150 or k>150:
	raise ValueError

valid_lists = 0
list = []
while len(list) < k:
	list.append(1)

# because of zero is not natural number, there cant be value higher than n-k+1
while list[0] < n-k+1:
	if sum(list) == n:
		print str(list)
		valid_lists += 1
	list_cell = list.index(list[len(list)-1])
	while list_cell != 0 and list[list_cell] == list[list_cell-1]:
		list_cell = list.index(list[list_cell])
	list[list_cell] += 1
	for index in xrange(list_cell+1, k):
		list[index] = 1

# last one may also be true
if sum(list) == n:
	print str(list)
	valid_lists += 1
print 'found ' + str(valid_lists) + ' valid solutions'
